#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

#define TIMER_MAX 9
typedef unsigned long long ullong;

#define SAMPLE
#ifdef SAMPLE
#define FILENAME "../sample.txt"
#define INPUT_TIMERS 5
#else
#define FILENAME "../input.txt"
#define INPUT_TIMERS 300
#endif

ullong* read_file() {
    FILE *file = fopen(FILENAME, "r");
    ullong *timers = malloc(sizeof(ullong) * TIMER_MAX);
    memset(timers, 0, sizeof(ullong) * TIMER_MAX);

    if (file == NULL)
    {
        fprintf(stderr, "fopen error \"%s\": %s\n",
          FILENAME,
          strerror(errno));
        exit(EXIT_FAILURE);
    }

    char* line = malloc(sizeof(char) * INPUT_TIMERS);
    while (fgets(line, sizeof(line), file)) {

        // Remove last breakline by setting it's pointer to 0
        char *newline = strchr(line, '\n');
        if (newline) {
            *newline = 0;
        }

        char *tmp = strtok(line, ",");
        while(tmp != NULL) {
            timers[atoi(tmp)]++;
            tmp = strtok(NULL, ",");
        }
    }

    fclose(file);
    return timers;
}

void print_timers(ullong* timers) {
    for (int i = 0; i < TIMER_MAX; ++i) {
        printf("%llu ", timers[i]);
    }
    printf("\n");
}

void iterate(ullong *timers, int steps) {
    for (int step = 0; step < steps; step++) {
        ullong timersZero = timers[0];
        for (int i = 1; i < TIMER_MAX; ++i) {
            timers[i - 1] = timers[i];
        }

        timers[TIMER_MAX - 3] += timersZero;
        timers[TIMER_MAX - 1] = timersZero;
        print_timers(timers);
    }
}

ullong count(ullong *timers) {
    ullong result = 0;
    for (int i = 0; i < TIMER_MAX; ++i) {
        result += timers[i];
    }

    return result;
}

int main(int argc, char *argv[])
{
    (void) argc;
    (void) argv;
    ullong *timers = read_file();
    print_timers(timers);
    iterate(timers, 256);
    ullong result = count(timers);
    printf("Result: %llu\n", result);
    free(timers);
    return 0;
}
