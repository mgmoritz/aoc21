const fs = require('fs')
const data = fs.readFileSync('input.txt', 'utf8')
      .split('\n')
      .map(i => parseInt(i))
      .filter(i => !!i)

console.log('part1', data.reduce((a, b, i, arr) => a + (arr[i] > arr[i-1] ? 1 : 0), 0))
console.log('part2', data.reduce((a, b, i, arr) => (i <= 2) ? a : a + (arr[i] > arr[i-3] ? 1 : 0), 0))
