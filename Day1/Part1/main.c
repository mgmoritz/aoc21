#include <stdio.h>
#include <stdlib.h>

#define NUMBER_OF_LINES 2000

// Read file as int array
int* read_file(char* filename) {
    FILE *file;
    file = fopen(filename, "r");

    static int depths[NUMBER_OF_LINES];

    for (int i = 0; i < NUMBER_OF_LINES; i++) {
        fscanf(file, "%d", &depths[i]);
    }

    return depths;
}

int count_increases(int* depths) {
    int result = 0;

    for(int i = 1; i < NUMBER_OF_LINES; i++) {
        printf("depths[i] %d > depths[i - 1] %d => %d\n",
          depths[i],
          depths[i - 1],
          (depths[i] > depths[i - 1]));
        result += (depths[i] > depths[i - 1]);
    }

    return result;
}

int main(int argc, char *argv[])
{
    int* depths = read_file("../input.txt");
    printf("n: %d\n", NUMBER_OF_LINES);
    printf("Number of increases: %d\n",
      count_increases(depths));

    (void) argc;
    (void) argv;

    return 0;
}
