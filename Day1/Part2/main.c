#include <stdio.h>

#define NUMBER_OF_LINES 2000

int* read_file(char* filename) {
    FILE *file;
    file = fopen(filename, "r");

    static int depths[NUMBER_OF_LINES];

    for (int i = 0; i < NUMBER_OF_LINES; i++) {
        fscanf(file, "%d", &depths[i]);
    }

    return depths;
}

int increasing_sliding_windows(int* depths) {
    int result = 0;

    for(int i = 3; i < NUMBER_OF_LINES; i++) {
        // This is the window definition
        int window1 = depths[i - 3] + depths[i - 2] + depths[i - 1];
        int window2 = depths[i - 2] + depths[i - 1] + depths[i];

        // the required comparison
        result += (depths[i] > depths[i - 3]);
        printf("window2 %d > window1 %d => %d\n",
          window2,
          window1,
          (window2 > window1));
    }

    return result;
}

int main(int argc, char *argv[])
{
    int* depths = read_file("../input.txt");
    printf("Number of increases: %d\n", increasing_sliding_windows(depths));
    (void) argc;
    (void) argv;
    return 0;
}
