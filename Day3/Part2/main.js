const fs = require('fs')
const data = fs.readFileSync('input.txt', 'utf8')
      .split('\n')
      .filter(i => !!i)
      .map(i => {
        return {
          raw: i,
          value: parseInt(i, 2)
        }
      })


const getO2 = (data) => {
  let O2Data= data.slice()
  for(let i = 0; i < data[0].raw.length; i++) {
    const O2DataElements = O2Data.length
    const mostCommon = O2Data.reduce((a, b) => {
      return a + parseInt(b.raw[i])
    }, 0) >= (O2DataElements / 2) ? 1 : 0
    O2Data= O2Data.filter((el, index, arr) => {
      return parseInt(el.raw[i]) === mostCommon || arr.length === 1
    })
  }

  return O2Data[0].value;
}

const getCO2 = (data) => {
  let CO2Data= data.slice()
  for(let i = 0; i < data[0].raw.length; i++) {
    const CO2DataElements = CO2Data.length
    const sum = CO2Data.reduce((a, b) => { return a + parseInt(b.raw[i]) }, 0)
    const leastCommon = sum >= (CO2DataElements / 2) ? 0 : 1
    CO2Data= CO2Data.filter((el, index, arr) => {
      return parseInt(el.raw[i]) === leastCommon || arr.length === 1
    })
  }

  return CO2Data[0].value;
}

const solve = (data) => {
  return getO2(data) * getCO2(data);
}

console.log('solve', solve(data))
