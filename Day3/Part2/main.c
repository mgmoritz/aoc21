#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

#define DEFAULT_OXYGEN_GENERATOR_RATING_VALUE 1
#define DEFAULT_CO2_SCRUBBER_RATING_VALUE 0

typedef struct report_entry {
    char* raw;
    int value;
    struct report_entry *next;
} report_entry;

typedef struct diagnostic_report {
    int size;
    report_entry *head;
} diagnostic_report;

diagnostic_report* init_diagnostic_report() {
    diagnostic_report* dr = malloc(sizeof(diagnostic_report));
    dr->size = 0;
    dr->head= NULL;

    return dr;
}

void print_entry(report_entry *re) {
    if(re == NULL) {
        printf("Empty Report Entry\n");
        return;
    }

    printf("Raw Binary Value: %s\n", re->raw);
    printf("Integer Value: %d\n", re->value);
    printf("Next Report Entry: %s\n", re->next == NULL ? "No" : "Yes");
}

int get_decimal_value(char* raw) {
    int length = strlen(raw);
    int value = 0;
    for (int i = 0; i < length; ++i) {
        int bit = raw[length - i - 1] - '0';
        int masked = bit << i;
        value += masked;
    }

    return value;
}

bool add_entry(diagnostic_report *dr, char* raw) {
    report_entry *re = malloc(sizeof(report_entry));

    if (re == NULL) {
        return false;
    }

    re->raw = malloc(strlen(raw) + 1);
    strcpy(re->raw, raw);

    re->value = get_decimal_value(raw);
    if (dr->head != NULL) {
        re->next = dr->head;
    }
    dr->head = re;
    return true;
}

bool delete_entry(diagnostic_report *dr, report_entry *re, report_entry *parent) {
    if (dr->head == re) {
        dr->head = re->next;
        return true;
    }

    parent->next = re->next;
    return true;
}

diagnostic_report* read_file(char* filename) {
    FILE *file;
    file = fopen(filename, "r");

    diagnostic_report *dr = init_diagnostic_report();
    char line[256];

    while (fgets(line, sizeof(line), file)) {
        char* binaryString = strtok(line, "\n");
        add_entry(dr, binaryString);
    }

    return dr;
}

int most_common_value(diagnostic_report *dr, int index, int defaultValue) {
    int sum = 0;
    int entriesCount = 0;

    report_entry *re = dr->head;
    while(re != NULL) {
        sum += re->raw[index] - '0';
        entriesCount++;
        re = re->next;
    }

    if (sum == (double) entriesCount / 2) return defaultValue;

    return sum > ((double) entriesCount / 2);
}

int least_common_value(diagnostic_report *dr, int index, int defaultValue) {
    int sum = 0;
    int entriesCount = 0;

    report_entry *re = dr->head;
    while(re != NULL) {
        sum += re->raw[index] - '0';
        entriesCount++;
        re = re->next;
    }

    if (sum == (double) entriesCount / 2) return defaultValue;

    return sum < ((double) entriesCount / 2);
}

int get_rank(diagnostic_report *dr) {
    report_entry *re = dr->head;
    int rank = 0;
    while(re != NULL) {
        rank++;
        re = re->next;
    }
    return rank;
}

bool duplicate_report_entry(report_entry* target, report_entry* src) {
    report_entry* re = malloc(sizeof(report_entry));
    re->raw = malloc(strlen(src->raw) + 1);
    strcpy(re->raw, src->raw);
    re->value = src->value;
    re->next = NULL;

    memcpy(target, re, sizeof(report_entry));
    return true;
}

diagnostic_report* copy_diagnostic_report(diagnostic_report *dr) {
    diagnostic_report *drLocal = malloc(sizeof(diagnostic_report));
    memcpy(drLocal, dr, sizeof(diagnostic_report));

    report_entry *srcRef = dr->head;
    report_entry *parent = malloc(sizeof(report_entry));
    duplicate_report_entry(parent, srcRef);
    drLocal->head = parent;

    report_entry *tmp= malloc(sizeof(report_entry));
    tmp = parent;
    int count = 0;

    while(srcRef->next != NULL) {
        if (count > 997) {
            report_entry *child = malloc(sizeof(report_entry));
            duplicate_report_entry(child, srcRef->next);
            tmp->next = child;
            srcRef = srcRef->next;
            tmp = child;
        } else {
            report_entry *child = malloc(sizeof(report_entry));
            duplicate_report_entry(child, srcRef->next);
            tmp->next = child;
            srcRef = srcRef->next;
            tmp = child;
        }
    }

    return drLocal;
}


int calculate_oxigen_generatory_rating(diagnostic_report *dr) {
    int arrSize = strlen(dr->head->raw);
    int *mostCommon = malloc(sizeof(int) * arrSize);
    diagnostic_report *drLocal = copy_diagnostic_report(dr);

    for (int i = 0; i < arrSize; i++) {
        int currentRank = get_rank(drLocal);
        mostCommon[i] = most_common_value(drLocal, i, DEFAULT_OXYGEN_GENERATOR_RATING_VALUE);
        report_entry *re = drLocal->head;
        report_entry *parent = NULL;
        while(re != NULL && currentRank > 1) {
            char value = re->raw[i] - '0';
            if (value != mostCommon[i]) {
                delete_entry(drLocal, re, parent);
                currentRank = get_rank(drLocal);
            } else {
                parent = re;
            }
            re = re->next;
        }
    }

    int result = get_decimal_value(drLocal->head->raw);
    free(drLocal);

    return result;
}

int calculate_co2_scrubber_rating(diagnostic_report *dr) {
    int arrSize = strlen(dr->head->raw);
    int *leastCommon = malloc(sizeof(int) * arrSize);
    diagnostic_report *drLocal = copy_diagnostic_report(dr);

    for (int i = 0; i < arrSize; i++) {
        int currentRank = get_rank(drLocal);
        leastCommon[i] = least_common_value(drLocal, i, DEFAULT_CO2_SCRUBBER_RATING_VALUE);
        report_entry *re = drLocal->head;
        report_entry *parent = NULL;
        while(re != NULL && currentRank > 1) {
            char value = re->raw[i] - '0';
            if (value != leastCommon[i]) {
                delete_entry(drLocal, re, parent);
                currentRank = get_rank(drLocal);
            } else {
                parent = re;
            }
            re = re->next;
        }
    }

    int result = get_decimal_value(drLocal->head->raw);
    free(drLocal);

    return result;
}

int main(int argc, char *argv[])
{
    (void) argc;
    (void) argv;

    diagnostic_report *dr = read_file("input.txt");
    int o2 = calculate_oxigen_generatory_rating(dr);
    int co2 = calculate_co2_scrubber_rating(dr);

    printf("Result: %d\n", o2 * co2);

    free(dr);
    return 0;
}
