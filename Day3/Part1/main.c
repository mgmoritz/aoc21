#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#define NUMBER_OF_LINES 1000
#define BIT_ARRAY_SIZE 12

typedef struct bits {
    char *raw;
    unsigned int value;
}bits;

bool nth_bit(unsigned int value, unsigned int nth) {
    int mask =  1 << nth;
    int maskedValue = value & mask;
    return maskedValue >> nth;
}

int get_gamma(bits* bs) {
    int sums[BIT_ARRAY_SIZE];
    for (int i = 0; i < BIT_ARRAY_SIZE; i++) {
        sums[i] = 0;
        for (int j = 0; j < NUMBER_OF_LINES; j++) {
            sums[i] += nth_bit(bs[j].value, i);
        }
    }

    int gamma = 0;
    for(int i = 0; i <= BIT_ARRAY_SIZE - 1; i++) {
        bool add = sums[i] > (NUMBER_OF_LINES / 2);
        int mask = 1 << i;
        gamma += mask * add;
    }

    return gamma;
}

int get_epsilon(bits* bs) {
    int sums[BIT_ARRAY_SIZE];
    for (int i = 0; i < BIT_ARRAY_SIZE; i++) {
        sums[i] = 0;
        for (int j = 0; j < NUMBER_OF_LINES; j++) {
            sums[i] += nth_bit(bs[j].value, i);
        }
    }

    int epsilon = 0;
    for(int i = 0; i <= BIT_ARRAY_SIZE - 1; i++) {
        bool add = sums[i] < (NUMBER_OF_LINES / 2);
        int mask = 1 << i;
        epsilon += mask * add;
    }

    return epsilon;
}

bits* read_file(char* filename) {
    FILE *file;
    file = fopen(filename, "r");

    static bits bs[NUMBER_OF_LINES];
    char line[256];
    int i = 0;
    while (fgets(line, sizeof(line), file)) {
        bs[i].raw = line;
        bs[i].value = (int) strtol(line, NULL, 2);
        i++;
    }
    fclose(file);
    return bs;
}

int main(int argc, char *argv[])
{
    bits* bs= read_file("input.txt");

    int result = get_gamma(bs) * get_epsilon(bs);
    printf("result: %d\n", result);

    (void) argc;
    (void) argv;
    return 0;
}
