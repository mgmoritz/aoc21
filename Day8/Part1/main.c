#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>

#define INPUT_LENGTH 100
#define DIGITS_LENGTH 8
#define NUMBER_OF_OUTPUT_DIGITS 4
#define DIGITS 10
#define PRODUCT_KEY_9 135
#define PRODUCT_KEY_6 576
#define PRODUCT_KEY_2 600
#define PRODUCT_KEY_0 280
#define PRODUCT_KEY_3 273
#define PRODUCT_KEY_5 495


/* #define SAMPLE */

#ifdef SAMPLE
#define FILENAME "../sample.txt"
#define INPUT_LINES 10
#else
#define FILENAME "../input.txt"
#define INPUT_LINES 200
#endif

typedef struct digit {
    char value[DIGITS_LENGTH];
    int map;
} digit;

typedef struct input_digits {
    digit digits[DIGITS];
} input_digits;

typedef struct output_digits {
    digit digits[NUMBER_OF_OUTPUT_DIGITS];
} output_digits;

typedef struct data {
    output_digits *outputs;
    input_digits *inputs;
} data;

char** read_file() {
    FILE *file = fopen(FILENAME, "r");

    if (file == NULL)
    {
        fprintf(stderr, "fopen error \"%s\": %s\n",
          FILENAME,
          strerror(errno));
        exit(EXIT_FAILURE);
    }

    char** inputs = malloc(sizeof(char*) * INPUT_LINES);

    char line[INPUT_LENGTH];
    int i = 0;
    while (fgets(line, sizeof(line), file)) {
        inputs[i] = malloc(sizeof(char) * INPUT_LENGTH);
        strcpy(inputs[i], line);
        i++;
    }

    fclose(file);
    return inputs;
}

void print_outputs(char** inputs) {
    for (int i = 0; i < INPUT_LINES; ++i) {
        printf("%s", inputs[i]);
    }
}

void print_input_digits(input_digits *inputs) {
    for (int i = 0; i < INPUT_LINES; ++i) {
        for (int j = 0; j < DIGITS; ++j) {
            printf("%s(%d) ", inputs[i].digits[j].value,
                inputs[i].digits[j].map);
        }
        printf("\n");
    }
}

void print_output_digits(output_digits *outputs) {
    for (int i = 0; i < INPUT_LINES; ++i) {
        for (int j = 0; j < NUMBER_OF_OUTPUT_DIGITS; ++j) {
            /* printf("%s ", outputs[i].digits[j].value); */
            printf("%d", outputs[i].digits[j].map);
        }
        printf("\n");
    }
}

void sort_digits(char* digit) {
    for (size_t i = 0; i < strlen(digit)- 1; i++) {
        for (size_t j = i; j < strlen(digit); j++) {
            if (digit[i] > digit[j]) {
                char tmp = digit[i];
                digit[i] = digit[j];
                digit[j] = tmp;
            }
        }
    }
}

void parse_segments(input_digits *iDigits) {
    for (int i = 0; i < DIGITS; ++i) {
        iDigits->digits[i].map = -1;
        char* v = iDigits->digits[i].value;
        int l = strlen(v);
        switch(l) {
        case 2:
            iDigits->digits[i].map = 1;
            break;
        case 3:
            iDigits->digits[i].map = 7;
            break;
        case 4:
            iDigits->digits[i].map = 4;
            break;
        case 5:
        case 6:
            break;
        case 7:
            iDigits->digits[i].map = 8;
            break;
        default:
            break;
        }
    }

    int* tmp_map= malloc(sizeof(int) * DIGITS);
    memset(tmp_map, -1, sizeof(int) * DIGITS);
    for (int i = 0; i < DIGITS; ++i) {
        if(iDigits->digits[i].map != -1)
            continue;

        int sum_puak = 0;
        int sum_pupk = 0;
        int sum_aupk = 0;

        /* printf("%s: %d\n", iDigits->digits[i].value, iDigits->digits[i].map); */
        for (int j = 0; j < DIGITS; ++j) {
            if (i == j || iDigits->digits[j].map == -1)
                continue;
            char *u= iDigits->digits[i].value;
            char *k= iDigits->digits[j].value;

            int uidx = 0;
            int kidx = 0;
            int ulen = strlen(u);
            int klen = strlen(k);

            // p: present
            // a: absent
            int puak = 0;
            int pupk = 0;
            int aupk = 0;

            while (uidx < ulen || kidx < klen) {
                if (uidx == ulen) {
                    aupk++;
                    kidx++;
                    continue;
                }
                if (kidx == klen) {
                    puak++;
                    uidx++;
                    continue;
                }
                if (k[kidx] > u[uidx]) {
                    puak++;
                    uidx++;
                    continue;
                }
                if (u[uidx] > k[kidx]) {
                    aupk++;
                    kidx++;
                    continue;
                }
                if (u[uidx] == k[kidx]) {
                    pupk++;
                    kidx++;
                    uidx++;
                    continue;
                }
            }

            sum_puak += puak;
            sum_pupk += pupk;
            sum_aupk += aupk;

            /* printf("k: %d\n", iDigits->digits[j].map); */
            /* printf("k: %s, puak: %d, pupk: %d, aupk: %d\n", k, puak, pupk, aupk); */
        }

        /* printf("sum_puak *sum_pupk * sum_aupk: %d\n", sum_puak * sum_pupk * sum_aupk); */

        int product = sum_puak * sum_pupk * sum_aupk;
        switch (product) {
        case PRODUCT_KEY_9:
            tmp_map[i] = 9;
            break;
        case PRODUCT_KEY_6:
            tmp_map[i] = 6;
            break;
        case PRODUCT_KEY_2:
            tmp_map[i] = 2;
            break;
        case PRODUCT_KEY_0:
            tmp_map[i] = 0;
            break;
        case PRODUCT_KEY_3:
            tmp_map[i] = 3;
            break;
        case PRODUCT_KEY_5:
            tmp_map[i] = 5;
            break;
        /* default: */
            /* printf("%d\n", product); */
        }
        /* printf("\n"); */
    }
    for (int i = 0; i < DIGITS; ++i) {
        if (iDigits->digits[i].map == -1)
            iDigits->digits[i].map = tmp_map[i];

        /* printf("%s: %d\n", iDigits->digits[i].value, iDigits->digits[i].map); */
    }
}

input_digits* parse_input(char* iString) {
    input_digits* iDigits = malloc(sizeof(input_digits));
    int i = 0;
    char* c = strtok(iString, " ");

    while(c != NULL) {
        sort_digits(c);
        /* printf("%s\n", c); */
        strcpy(iDigits->digits[i].value, c);
        i++;
        c = strtok(NULL, " ");
    }

    /* printf("\n"); */
    return iDigits;
}

output_digits* parse_output(char* oString) {
    output_digits* oDigits = malloc(sizeof(output_digits));
    int i = 0;
    char* c = strtok(oString, " ");

    while(c != NULL) {
        sort_digits(c);
        /* printf("%s\n", c); */
        strcpy(oDigits->digits[i].value, c);
        c = strtok(NULL, " ");
        i++;
    }
    /* printf("\n"); */
    return oDigits;
}

void copy_maps(output_digits *oDigits, input_digits *iDigits) {
    int N_IN = DIGITS;
    int N_OUT = NUMBER_OF_OUTPUT_DIGITS;
    for (int o = 0; o < N_OUT; ++o) {
        for (int i = 0; i < N_IN; ++i) {
            if (strcmp (oDigits->digits[o].value, iDigits->digits[i].value) == 0) {
                oDigits->digits[o].map = iDigits->digits[i].map;
            }
        }
    }

}

data* parse_data(char** inputs) {
    data *d = malloc(sizeof(data));
    d->inputs = malloc(sizeof(input_digits) * INPUT_LINES);
    d->outputs = malloc(sizeof(output_digits) * INPUT_LINES);

    for (int i = 0; i < INPUT_LINES; ++i) {
        char *newline = strchr(inputs[i], '\n');
        if (newline)
            *newline = 0;
        char* iString = strtok(inputs[i], "|");
        char* oString = strtok(NULL, "|");
        /* printf("is %s\n", iString); */
        /* printf("os %s\n", oString); */
        input_digits* iDigits = parse_input(iString);
        parse_segments(iDigits);
        output_digits* oDigits = parse_output(oString);
        copy_maps(oDigits, iDigits);

        d->inputs[i] = *iDigits;
        d->outputs[i] = *oDigits;
    }

    return d;
}

int count_known_values(output_digits *outputs) {
    int result = 0;
    for (int i = 0; i < INPUT_LINES; ++i) {
        for (int j = 0; j < NUMBER_OF_OUTPUT_DIGITS; ++j) {
            // 1(2), 4(4), 7(3), and 8(7)
            int l = strlen(outputs[i].digits[j].value);
            int number = -1;
            if (l == 2 || l == 3 || l == 4 || l == 7) {
                switch(l) {
                case 2:
                    number = 1;
                    break;
                case 3:
                    number = 7;
                    break;
                case 4:
                    number = 4;
                    break;
                case 7:
                    number = 8;
                    break;
                default:
                    break;
                }
                /* printf("Digit: %d, code: %s\n", */
                /*   number, */
                /*   outputs[i].digits[j].value); */
                result++;
            }
        }
    }

    return result;
}

int sum_outputs(output_digits *outputs) {
    int L = INPUT_LINES;
    int N_OUT = NUMBER_OF_OUTPUT_DIGITS;
    int sum = 0;

    for (int l = 0; l < L; ++l) {
        int v = 0;
        int multiplier = 1000;

        for (int o = 0; o < N_OUT; ++o) {
            v += multiplier * outputs[l].digits[o].map;
            multiplier /= 10;
        }
        sum += v;
    }

    return sum;
}

int main(int argc, char *argv[])
{
    char** input = read_file();
    /* print_outputs(input); */
    data *d = parse_data(input);
    /* print_input_digits(d->inputs); */
    /* print_output_digits(d->outputs); */
    /* int result = count_known_values(d->outputs); */
    /* printf("%d\n", result); */
    printf("Result %d\n", sum_outputs(d->outputs));
    (void) argc;
    (void) argv;
    return 0;
}
