#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#define K1 13
#define K2 29

/* #define SAMPLE */

#ifdef SAMPLE
#define FILENAME "../sample.txt"
#define N 18
#define LENGTH 20
#define SEED_LENGTH 4
#else
#define FILENAME "../input.txt"
#define N 102
#define LENGTH 20
#define SEED_LENGTH 20
#endif

typedef struct rule {
    char c1;
    char c2;
    char c3;
} rule;

char** read_file() {

    FILE *file = fopen(FILENAME, "r");

    if (file == NULL)
    {
        fprintf(stderr, "fopen error \"%s\": %s\n",
          FILENAME,
          strerror(errno));
        exit(EXIT_FAILURE);
    }

    char** lines = malloc(sizeof(char*) * N);
    char line[LENGTH * 2];
    int i = 0;

    while (fgets(line, sizeof(line), file)) {
        char *newline = strchr(line, '\n');
        if (newline) {
            *newline = 0;
        }

        lines[i] = malloc(sizeof(char) * (LENGTH + 1));
        strcpy(lines[i], line);
        i++;
    }

    return lines;
}

void print_data(char** data) {
    for (int i = 0; i < N; ++i) {
        printf("%s\n", data[i]);
    }
}

char* get_seed(char** data) {
    return data[0];
}

rule parse_rule(char* str_rule) {
    rule r = {
        .c1 = str_rule[0],
        .c2 = str_rule[1],
        .c3 = str_rule[6],
    };

    return r;
}

rule* get_rules(char **data) {
    int n_rules = N - 2;
    int rule_array_size = K1 * 'Z' * K2 * 'Z';
    char** str_rules = malloc(sizeof(char*) * n_rules);
    rule* rules = malloc(sizeof(rule) * n_rules);
    memcpy(str_rules, &data[2], sizeof(char*) * n_rules);
    for (int i = 0; i < n_rules; ++i) {
        rule r = parse_rule(str_rules[i]);
        int idx = r.c1 * K1 + r.c2 * K2;
        rules[idx] = r;
    }

    return rules;
}

void print_rules(rule* rules) {
    for (int i = 0; i < (N - 2); ++i) {
        printf("%c%c -> %c\n", rules[i].c1, rules[i].c2, rules[i].c3);
    }
}

char* solve(char *seed, rule* rules, int iterations) {
    char *result = malloc(sizeof(char) * 700000);
    strcpy(result, seed);

    for (int i = 0; i < iterations; ++i) {
        char *j_seed = malloc(sizeof(char) * 700000);
        char *j_insert = malloc(sizeof(char) * 700000);
        strcpy(j_seed, result);
        int j_length = strlen(j_seed);
        /* printf("%d\n", j_length); */

        for(int j = 1; j < j_length; j++) {
            char c3 = rules[j_seed[j - 1] * K1 + j_seed[j] * K2].c3;
            strncat(j_insert, &c3, 1);
        }

        for (int m = 0; m < 2 * j_length - 1; ++m) {
            if (m % 2 == 0) {
                result[m] = j_seed[m / 2];
            } else {
                result[m] = j_insert[m / 2];
            }
        }
        /* printf("result concat: %s \n", result); */
        /* free(j_seed); */
        /* free(j_insert); */
    }

    /* printf("%s\n", result); */
    return result;
}

int count_occurrence(char *sequence, char c) {
    int n = 0;
    for (size_t i = 0; i < strlen(sequence); i++) {
        if (sequence[i] == c)
            n++;
    }

    return n;
}

int main(int argc, char *argv[])
{
    (void) argc;
    (void) argv;
    char** data = read_file();

    /* print_data(data); */
    char* seed = get_seed(data);
    rule* rules = get_rules(data);
    /* printf("seed: %s\n", seed); */
    /* printf("rules: \n"); */
    /* print_rules(rules); */
    char* solution = solve(seed, rules, 14);
    /* printf("%s\n", solution); */
    int min = INT_MAX;
    int max = INT_MIN;

    for (size_t i = 0; i < strlen(solution); ++i) {
        int n = count_occurrence(solution, solution[i]);
        if (n > max)
            max = n;
        if (n < min)
            min = n;
    }

    printf("max: %d\n", max);
    printf("min: %d\n", min);

    printf("result: %d\n", max - min);
    return 0;
}
