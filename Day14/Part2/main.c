#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>

#define K1 13
#define K2 29
#define EMPTY_STACK '!'
#define STEPS 10

#define SAMPLE

#ifdef SAMPLE
#define FILENAME "../sample.txt"
#define N 18
#define LENGTH 20
#define SEED_LENGTH 4
#else
#define FILENAME "../input.txt"
#define N 102
#define LENGTH 20
#define SEED_LENGTH 20
#endif

// The performance I got in part one was not enough.
// I suspect that the issue is the array rewrite.
// I will not profile (bad idea?)
// I will try to implement a linked-list to store the solution

typedef struct rule {
    char c1;
    char c2;
    char c3;
    char** steps;
} rule;

char** read_file() {

    FILE *file = fopen(FILENAME, "r");

    if (file == NULL)
    {
        fprintf(stderr, "fopen error \"%s\": %s\n",
          FILENAME,
          strerror(errno));
        exit(EXIT_FAILURE);
    }

    char** lines = malloc(sizeof(char*) * N);
    char line[LENGTH * 2];
    int i = 0;

    while (fgets(line, sizeof(line), file)) {
        char *newline = strchr(line, '\n');
        if (newline) {
            *newline = 0;
        }

        lines[i] = malloc(sizeof(char) * (LENGTH + 1));
        strcpy(lines[i], line);
        i++;
    }

    return lines;
}

void print_data(char** data) {
    for (int i = 0; i < N; ++i) {
        printf("%s\n", data[i]);
    }
}

char* get_seed(char** data) {
    return data[0];
}

rule parse_rule(char* str_rule) {
    rule r = {
        .c1 = str_rule[0],
        .c2 = str_rule[1],
        .c3 = str_rule[6],
    };

    r.steps = malloc(sizeof(char*) * STEPS);
    r.steps[0] = malloc(sizeof(char) * 3);
    r.steps[0][0] = r.c1;
    r.steps[0][1] = r.c2;

    r.steps[1] = malloc(sizeof(char) * 4);
    r.steps[1][0] = r.c1;
    r.steps[1][1] = r.c2;
    r.steps[1][2] = r.c3;

    return r;
}

rule* get_rules(char **data) {
    int n_rules = N - 2;
    int rule_array_size = K1 * 'Z' * K2 * 'Z';
    char** str_rules = malloc(sizeof(char*) * n_rules);
    rule* rules = malloc(sizeof(rule) * rule_array_size);
    memcpy(str_rules, &data[2], sizeof(char*) * n_rules);
    for (int i = 0; i < n_rules; ++i) {
        rule r = parse_rule(str_rules[i]);
        int idx = r.c1 * K1 + r.c2 * K2;
        rules[idx] = r;
        /* printf("idx(%d) %c, %c: %c\n", r.c1 * K1 + r.c2 * K2, r.c1, r.c2, r.c3); */
    }

    return rules;
}

void print_rules(rule* rules) {
    for (int i = 0; i < (N - 2); ++i) {
        printf("%c%c -> %c\n", rules[i].c1, rules[i].c2, rules[i].c3);
    }
}

void print_seed_rules(rule **rules, int seed_length) {
    for (int i = 0; i < seed_length - 1; ++i) {
        printf("%c%c -> %c\n", rules[i]->c1, rules[i]->c2, rules[i]->c3);
    }
}

rule **seed_to_rules(char *seed, rule *rules) {
    // NNCB -> NN NC CB
    int seed_length = strlen(seed);
    rule **seed_rules = malloc(sizeof(rule*) * seed_length - 1);

    for (int i = 0; i < seed_length - 1; ++i) {
        seed_rules[i] = &rules[seed[i + 1] * K2 + seed[i] * K1];
    }

    return seed_rules;
}

char* solve(char *seed, rule* rules, int iterations) {
    rule **seed_rules = seed_to_rules(seed, rules);
    int seed_length = strlen(seed);
    print_seed_rules(seed_rules, seed_length);

    /* for (int i = 0; i < iterations; ++i) { */
        /* while(next->next != NULL) { */
            /* char c3 = rules[next->value * K2 + next->next->value * K1].c3; */
            /* push_at(c3, next); */
            /* next = next->next->next; */
        /* } */
    /* } */

    return "";
}

int count_occurrence(char *sequence, char c) {
    int n = 0;
    for (size_t i = 0; i < strlen(sequence); i++) {
        if (sequence[i] == c)
            n++;
    }

    return n;
}

void part_two() {
    char** data = read_file();

    /* print_data(data); */
    char* seed = get_seed(data);
    rule* rules = get_rules(data);
    /* printf("seed: %s\n", seed); */
    /* printf("rules: \n"); */
    /* print_rules(rules); */
    char* solution = solve(seed, rules, STEPS);
    /* printf("%s\n", solution); */
    int min = INT_MAX;
    int max = INT_MIN;

    /* for (size_t i = 0; i < strlen(solution); ++i) { */
    /*     int n = count_occurrence(solution, solution[i]); */
    /*     if (n > max) */
    /*         max = n; */
    /*     if (n < min) */
    /*         min = n; */
    /* } */

    /* printf("max: %d\n", max); */
    /* printf("min: %d\n", min); */

    /* printf("result: %d\n", max - min); */
}

int main(int argc, char *argv[]) {
    (void) argc;
    (void) argv;

    /* test_linked_list(); */
    part_two();
    return 0;
}
