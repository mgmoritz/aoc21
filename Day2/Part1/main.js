const fs = require('fs')
const data = fs.readFileSync('input.txt', 'utf8')
      .split('\n')
      .filter(i => !!i)
      .map(i => {
        const s = i.split(' ')
        return { command: s[0], amount: parseInt(s[1])}
      })

const solve = (data) => {
  let hPos = 0
  let depth = 0

  data.forEach(i => {
    switch (i.command) {
    case 'forward':
      hPos += i.amount
      break
    case 'down':
      depth += i.amount
      break
    case 'up':
      depth -= i.amount
      break
    default:
      console.error(`Invalid command i.command`);
    }
  })
  return hPos * depth
}

console.log('solve()', solve(data))
