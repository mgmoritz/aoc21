#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NUMBER_OF_LINES 1000
#define DOWN 0
#define UP 1
#define FORWARD 2

typedef struct instruction {
    int direction;
    int value;
} instruction;

instruction* init_instruction(int direction, int value) {
    instruction* i = malloc(sizeof(instruction));
    i->direction = direction;
    i->value = value;

    return i;
}

int get_direction(char* instructionName) {
    switch (instructionName[0]) {
    case 0x66:
        return FORWARD;
    case 0x64:
        return DOWN;
    case 0x75:
        return UP;
    default:
        return -1;
    }
}

instruction* read_file(char* filename) {
    FILE *file;
    file = fopen(filename, "r");

    static instruction instructions[NUMBER_OF_LINES];
    char line[256];
    int i = 0;

    while (fgets(line, sizeof(line), file)) {
        char *instructionName = strtok(line, " ");
        char *instructionValue = strtok(NULL, " ");

        int iName = get_direction(instructionName);
        int iValue = atoi(instructionValue);

        instruction *ins = init_instruction(iName, iValue);
        instructions[i].value = ins->value;
        instructions[i].direction = ins->direction;
        i++;
    }
    fclose(file);
    return instructions;
}

int solve(instruction* instructions) {
    int aim = 0;
    int hPos = 0;
    int depth = 0;

    for (int i = 0; i < NUMBER_OF_LINES; i++) {
        switch (instructions[i].direction) {
        case FORWARD:
            hPos += instructions[i].value;
            depth += (aim * instructions[i].value);
            break;
        case DOWN:
            aim += instructions[i].value;
            break;
        case UP:
            aim -= instructions[i].value;
            break;
        default:
            return 0;
        }
    }

    return hPos * depth;
}

int main(int argc, char *argv[])
{
    instruction* instructions = read_file("input.txt");
    printf("%d\n", solve(instructions));

    (void) argc;
    (void) argv;
    return 0;
}
