#include <errno.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#define MAX_DRAWS 100
#define MAX_BOARDS 100
#define BOARD_ROWS 5
#define BOARD_COLUMNS 5
#define BOARD_SIZE BOARD_ROWS * BOARD_COLUMNS
#define BOARD_NUMBER_SIZE sizeof(int) * BOARD_SIZE

typedef struct board {
    int boardNumber;
    int *numbers;
    bool *marked;
    bool won;
} board;

typedef struct bingo {
    board *boards;
    int *draws;
    int drawIndex;
} bingo;

bingo* init_bingo() {
    bingo *b = malloc(sizeof(bingo));
    b->drawIndex = -1;
    return b;
}

void print_numbers(int* numbers) {
    for (int i = 0; i < BOARD_SIZE; ++i) {
        if (i % BOARD_COLUMNS == 0)
            printf("%s\n", "");
        printf("%2d ", numbers[i]);
    }
    printf("%s\n", "");
}

board* init_board(int index, int* numbers) {
    /* print_numbers(numbers); */
    board *b = malloc(sizeof(board));
    b->boardNumber = index;
    b->numbers = malloc(BOARD_NUMBER_SIZE);
    memcpy(b->numbers, numbers, BOARD_NUMBER_SIZE);
    b->marked = malloc(BOARD_NUMBER_SIZE);
    b->won = 0;
    memset(b->marked, 0, BOARD_NUMBER_SIZE);
    return b;
}

void print_board(board *board) {
    printf("Board Number: %d\n", board->boardNumber);
    for (int i = 0; i < BOARD_SIZE; ++i) {
        if (board->marked[i]) {
            printf("%s", "\x1B[31m"); // set marked
        }
        printf("%2d ", board->numbers[i]);
        printf("%s", "\x1B[0m"); // set normal
        if (i % BOARD_COLUMNS == BOARD_COLUMNS - 1)
            printf("%s\n", "");
    }
    printf("%s\n", "");

}

void print_bingo(bingo *bingo) {
    for (int i = 0; i < MAX_BOARDS; ++i) {
        board *b = &bingo->boards[i];
        if (b->numbers != NULL)
            print_board(b);
    }
}

int* parse_board(char* line) {
    int *numbers = malloc(BOARD_NUMBER_SIZE);
    memset(numbers, -1, BOARD_NUMBER_SIZE);
    char* n = strtok(line, " ");
    int counter = 0;

    while(n != NULL) {
        numbers[counter] = atoi(n);
        counter++;
        n = strtok(NULL, " ");
    }

    return numbers;
}

int* parse_draws(char* line) {
    int *draws = malloc(sizeof(int) * MAX_DRAWS);
    memset(draws, -1, sizeof(int) * MAX_DRAWS);
    char* n = strtok(line, ",");
    int counter = 0;

    while(n != NULL) {
        draws[counter] = atoi(n);
        printf("%d ", draws[counter]);
        counter++;
        n = strtok(NULL, ",");
    }
    printf("\n\n");

    return draws;
}

int count_valid_numbers(int *numbers, int size) {
    int count = 0;
    for(int i = 0; i < size; i++) {
        if (numbers[i] != -1) {
            count++;
        }
    }

    return count;
}

int get_score(board *board, int lastDraw) {
    int sumUnmarked = 0;
    for (int j = 0; j < BOARD_SIZE; ++j) {
        if (!board->marked[j]) {
            sumUnmarked += board->numbers[j];
        }
    }
    printf("lastDraw %d\n", lastDraw);
    printf("sumUnmarked %d\n", sumUnmarked);
    return lastDraw * sumUnmarked;
}

board* get_winner(bingo* bingo) {
    board* winner = NULL;

    for (int boardIndex = 0; boardIndex < MAX_BOARDS; ++boardIndex) {
        board *bs = &bingo->boards[boardIndex];
        if (bs->numbers == NULL)
            break;

        if (bs->won)
            continue;

        for (int i = 0; i < BOARD_ROWS; ++i) {
            bool rowWin = true;
            for (int j = 0; j < BOARD_COLUMNS; ++j) {
                rowWin &= bs->marked[i * BOARD_COLUMNS + j];
            }
            if (rowWin) {
                winner = bs;
                printf("Row Win.\n");
                bs->won = true;
                printf("Board %d won with score of %d in the row %d!\n",
                  winner->boardNumber, get_score(winner, bingo->draws[bingo->drawIndex]), i);
            }
        }

        for (int j = 0; j < BOARD_COLUMNS; ++j) {
            bool columnWin = true;
            for (int i = 0; i < BOARD_ROWS; ++i) {
                columnWin &= bs->marked[i * BOARD_COLUMNS + j];
            }
            if (columnWin) {
                winner = bs;
                printf("Column Win.\n");
                bs->won = true;
                printf("Board %d won with score of %d in the column %d!\n",
                  winner->boardNumber, get_score(winner, bingo->draws[bingo->drawIndex]), j);
            }
        }
    }

    return winner;
}

void draw_numbers(bingo* bingo, int n) {
    for (int i = 0; i < n; i++) {
        if (bingo->draws[i] == -1) {
            return;
        }
        printf("Drawing %d.\n", bingo->draws[i]);
        for (int boardIndex = 0; boardIndex < MAX_BOARDS; ++boardIndex) {
            board *bs = &bingo->boards[boardIndex];
            if (bs->numbers == NULL)
                break;
            if (bs->won) {
                continue;
            }
            for (int j = 0; j < BOARD_SIZE; ++j) {
                if (bs->numbers[j] == bingo->draws[i]) {
                    bs->marked[j] = true;
                }
            }
        }
        bingo->drawIndex++;
        board *winner = get_winner(bingo);
        if (winner != NULL) {
            /* break; */
        }
    }
}

bingo* read_file(char* filename) {
    FILE *file;
    file = fopen(filename, "r");
    if (file == NULL)
    {
        fprintf(stderr, "fopen error \"%s\": %s\n",
          filename,
          strerror(errno));
        exit(EXIT_FAILURE);
    }

    bingo *bingo = init_bingo();
    char line[512];
    int lineNumber = 0;
    int* draws;
    int* boardNumbers = malloc(BOARD_NUMBER_SIZE);
    memset(boardNumbers, -1, BOARD_NUMBER_SIZE);
    board* boards = malloc(sizeof(board) * MAX_BOARDS);
    int boardNumber = 0;


    while (fgets(line, sizeof(line), file)) {
        lineNumber++;
        if (lineNumber == 1) {
            draws = parse_draws(line);
        }
        else if (lineNumber == 2) {
            // pass
        }
        else if (lineNumber % 6 == 2) {
            /* printf("\n"); */
            board *b = init_board(boardNumber, boardNumbers);
            boards[boardNumber] = *b;
            boardNumber++;
            memset(boardNumbers, -1, BOARD_NUMBER_SIZE);
        } else {
            /* printf("%s", line); */
            int *numbersFromLine = parse_board(line);
            int pos = count_valid_numbers(boardNumbers, BOARD_SIZE);
            for (int i = 0; i < BOARD_COLUMNS; i++) {
                boardNumbers[pos + i] = numbersFromLine[i];
            }
        }
    }

    // set the last board
    board *b = init_board(boardNumber, boardNumbers);
    boards[boardNumber] = *b;

    bingo->draws = draws;
    bingo->boards = boards;

    /* print_bingo(bingo); */

    return bingo;
}

int main()
{
    bingo* bingo = read_file("../input.txt");
    draw_numbers(bingo, 100);
    /* print_bingo(bingo); */
    return 0;
}
