#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>

/* #define SAMPLE */

#ifdef SAMPLE
#define FILENAME "../sample.txt"
#define ROWS 10
#define COLUMNS 10
#else
#define FILENAME "../input.txt"
#define ROWS 10
#define COLUMNS 10
#endif

char** read_file() {

    FILE *file = fopen(FILENAME, "r");

    if (file == NULL)
    {
        fprintf(stderr, "fopen error \"%s\": %s\n",
          FILENAME,
          strerror(errno));
        exit(EXIT_FAILURE);
    }

    char** lines = malloc(sizeof(char*) * ROWS);
    char line[COLUMNS * 2];
    int i = 0;

    while (fgets(line, sizeof(line), file)) {
        char *newline = strchr(line, '\n');
        if (newline) {
            *newline = 0;
        }

        lines[i] = malloc(sizeof(char) * (COLUMNS + 1));
        strcpy(lines[i], line);
        i++;
    }

    return lines;
}

void print_data(char** data) {
    for (int i = 0; i < ROWS; ++i) {
        printf("%s\n", data[i]);
    }
}

int** parse_data(char** data) {
    int** lines = malloc(sizeof(int*) * ROWS);
    for (int r = 0; r < ROWS; ++r) {
        lines[r] = malloc(sizeof(int) * COLUMNS);
        for (int c = 0; c < COLUMNS; ++c) {
            lines[r][c] = data[r][c] - '0';
        }
    }
    return lines;
}

void print_energy(int** e) {
    for (int i = 0; i < ROWS; ++i) {
        for (int j = 0; j < COLUMNS; j++) {
            if (e[i][j] == 9) {
                printf("%s", "\x1B[32m"); // set marked
            }
            if (e[i][j] == 0) {
                printf("%s", "\x1B[31m"); // set marked
            }
            printf("%d", e[i][j]);
            printf("%s", "\x1B[0m"); // set normal
        }
        printf("\n");
    }
    printf("\n");
}

bool out_of_bounds(int r, int c) {
    return (r < 0 || r >= ROWS || c < 0 || c >= COLUMNS);
}

int flash(int** e, int r, int c) {
    int n_flashes = 1;
    for (int neigh_row = -1; neigh_row <= 1; ++neigh_row) {
        for (int neigh_column = -1; neigh_column <= 1; ++neigh_column) {
            int row = r + neigh_row;
            int column= c + neigh_column;
            if (out_of_bounds(row, column))
                continue;
            e[row][column]++;
            if (e[row][column] == 10) {
                n_flashes += flash(e, row, column);
            }
        }
    }

    return n_flashes;
}

int solve(int** e, int n) {
    int n_flashes = 0;
    for (int i = 0; i < n; ++i) {
        for (int r = 0; r < ROWS; ++r) {
            for (int c = 0; c < COLUMNS; c++) {
                e[r][c]++;
                if (e[r][c] == 10) {
                    n_flashes += flash(e, r, c);
                }
            }
        }
        for (int r = 0; r < ROWS; ++r) {
            for (int c = 0; c < COLUMNS; c++) {
                if (e[r][c] > 9) {
                    e[r][c] = 0;
                }
            }
        }
        printf("After step %d:\n", i + 1);
        print_energy(e);
    }

    return n_flashes;
}

// what I should do
// iterates on all cells, add 1 then, if flashes, call flash.
// flash should iterate on all neighbors and add one,
// if the neighbors flashes, call flash
int main(int argc, char *argv[])
{
    (void) argc;
    (void) argv;
    char** data = read_file();

    /* print_data(data); */

    int** energy = parse_data(data);
    print_energy(energy);
    int iterations = 100;
    int result = solve(energy, iterations);
    printf("%d\n", result);
    return 0;
}
