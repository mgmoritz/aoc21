#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>

/* #define SAMPLE */

#ifdef SAMPLE
#define FILENAME "../sample.txt"
#define N 10
#define LENGTH 25
#else
#define FILENAME "../input.txt"
#define N 110
#define LENGTH 110
#endif

#define STACK_LENGHT LENGTH
#define STACK_EMPTY -1
#define STACK_EMPTY_VALUE 'E'

typedef unsigned long long llu;

typedef struct stack {
    char values[STACK_LENGHT];
    int corruption;
    llu completion_score;
    int top;
} stack;

stack *init_stack() {
    stack *s = malloc(sizeof(stack));
    s->top = STACK_EMPTY;
    s->corruption = 0;
    s->completion_score = 0;

    return s;
}

bool push(stack *stack, char value) {
    if (stack->top >= STACK_LENGHT - 1) {
        return false;
    }
    stack->top++;
    stack->values[stack->top] = value;

    return true;
}

int pop(stack *stack) {
    if (stack->top == STACK_EMPTY) {
        return STACK_EMPTY_VALUE;
    }

    return stack->values[stack->top--];
}

void print_stack(stack *stack) {
    for(int i = 0; i < (stack->top + 1); i++) {
        printf("%c", stack->values[i]);
    }

    printf("\n");
}

int count(stack *stack) {
    return stack->top + 1;
}

char** read_file() {

    FILE *file = fopen(FILENAME, "r");

    if (file == NULL)
    {
        fprintf(stderr, "fopen error \"%s\": %s\n",
          FILENAME,
          strerror(errno));
        exit(EXIT_FAILURE);
    }

    char** lines = malloc(sizeof(char*) * N);
    char line[LENGTH * 2];
    int i = 0;

    while (fgets(line, sizeof(line), file)) {
        char *newline = strchr(line, '\n');
        if (newline) {
            *newline = 0;
        }

        lines[i] = malloc(sizeof(char) * (LENGTH + 1));
        strcpy(lines[i], line);
        i++;
    }

    return lines;
}

void print_data(char** data) {
    for (int i = 0; i < N; ++i) {
        printf("%s\n", data[i]);
    }
}

bool is_pair(char a, char b) {
    switch(a) {
    case '(':
        return b == ')';
    case '{':
        return b == '}';
    case '<':
        return b == '>';
    case '[':
        return b == ']';
    default:
        return false;
    }
}

bool is_closing(char c) {
    switch(c) {
    case ')':
    case '}':
    case '>':
    case ']':
        return true;
    default:
        return false;
    }
}

int corruption_value(char c) {
    switch(c) {
    case ')':
        return 3;
    case ']':
        return 57;
    case '}':
        return 1197;
    case '>':
        return 25137;
    default:
        return 0;
    }
}

stack *parse_line(char *line) {
    stack *s = init_stack();
    for (size_t i = 0; i < strlen(line); i++) {
        char last = pop(s);
        if (last == STACK_EMPTY_VALUE) {
            push(s, line[i]);
            continue;
        }
        if (is_pair(last, line[i])) {
            /* printf("Found a pair at %3d: %c and %c\n", i, last, line[i]); */
            /* push(s, last); */
            /* push(s, line[i]); */
        } else if(is_closing(line[i])) {
            /* printf("Found corruption at %3d: %c and %c\n", i, last, line[i]); */
            s->corruption = corruption_value(line[i]);
            break;
        } else {
            push(s, last);
            push(s, line[i]);
        }
    }
    return s;
}

stack **parse_stacks(char **data) {
    stack **ss = malloc(sizeof(stack*) * N);
    for (int i = 0; i < N; ++i) {
        stack *s = parse_line(data[i]);
        ss[i] = s;
    }

    return ss;
}

int sum_corruption(stack **stacks) {
    int sum = 0;
    for (int i = 0; i < N; ++i) {
        sum += stacks[i]->corruption;
    }

    return sum;
}

int add_score(char c) {
    switch(c) {
    case '(':
        return 1;
    case '[':
        return 2;
    case '{':
        return 3;
    case '<':
        return 4;
    default:
        return 0;
    }
}

void sort_int(llu* values, int size) {
    for (int i = 0; i < size - 1; i++) {
        for (int j = i; j < size; j++) {
            if (values[j] > values[i]) {
                llu tmp = values[i];
                values[i] = values[j];
                values[j] = tmp;
            }
        }
    }
}

int complete_stacks(stack **stacks) {
    int count_not_corrupted = 0;
    llu* completion_scores = malloc(sizeof(llu) * N);
    memset(completion_scores, 0, sizeof(llu) * N);
    for (int i = 0; i < N; ++i) {
        if (stacks[i]->corruption == 0) {
            char c = pop(stacks[i]);
            while(c != STACK_EMPTY_VALUE) {
                stacks[i]->completion_score *= 5;
                stacks[i]->completion_score += add_score(c);
                c = pop(stacks[i]);
            }
            completion_scores[count_not_corrupted] = stacks[i]->completion_score;
            count_not_corrupted++;
            printf("score[%d]: %llu\n", i, stacks[i]->completion_score);
        }
    }

    sort_int(completion_scores, count_not_corrupted);
    return completion_scores[(count_not_corrupted / 2)];
}

void print_stacks_values(stack **ss) {
    for (int i = 0; i < N; ++i) {
        printf("Stack %3d:\n", i);
        for (int j = 0; j < ss[i]->top; ++j) {
            printf("%c", ss[i]->values[j]);
        }
        printf("\n");
    }

}

int main(int argc, char *argv[])
{
    (void) argc;
    (void) argv;
    char** data = read_file();
    stack **stacks = parse_stacks(data);
    /* print_stacks_values(stacks); */
    int result = complete_stacks(stacks);
    printf("\nResult: %d\n\n", result);
    return 0;
}
