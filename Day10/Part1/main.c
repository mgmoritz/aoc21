#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>

/* #define SAMPLE */

#ifdef SAMPLE
#define FILENAME "../sample.txt"
#define N 10
#define LENGTH 25
#else
#define FILENAME "../input.txt"
#define N 110
#define LENGTH 110
#endif

#define STACK_LENGHT LENGTH
#define STACK_EMPTY -1
#define STACK_EMPTY_VALUE 'E'

typedef struct stack {
    char values[STACK_LENGHT];
    int corruption;
    int top;
} stack;

stack *init_stack() {
    stack *s = malloc(sizeof(stack));
    s->top = STACK_EMPTY;
    s->corruption = 0;

    return s;
}

bool push(stack *stack, char value) {
    if (stack->top >= STACK_LENGHT - 1) {
        return false;
    }
    stack->top++;
    stack->values[stack->top] = value;

    return true;
}

int pop(stack *stack) {
    if (stack->top == STACK_EMPTY) {
        return STACK_EMPTY_VALUE;
    }

    return stack->values[stack->top--];
}

void print_stack(stack *stack) {
    for(int i = 0; i < (stack->top + 1); i++) {
        printf("%c", stack->values[i]);
    }

    printf("\n");
}

int count(stack *stack) {
    return stack->top + 1;
}

char** read_file() {

    FILE *file = fopen(FILENAME, "r");

    if (file == NULL)
    {
        fprintf(stderr, "fopen error \"%s\": %s\n",
          FILENAME,
          strerror(errno));
        exit(EXIT_FAILURE);
    }

    char** lines = malloc(sizeof(char*) * N);
    char line[LENGTH * 2];
    int i = 0;

    while (fgets(line, sizeof(line), file)) {
        char *newline = strchr(line, '\n');
        if (newline) {
            *newline = 0;
        }

        lines[i] = malloc(sizeof(char) * (LENGTH + 1));
        strcpy(lines[i], line);
        i++;
    }

    return lines;
}

void print_data(char** data) {
    for (int i = 0; i < N; ++i) {
        printf("%s\n", data[i]);
    }
}

bool is_pair(char a, char b) {
    switch(a) {
    case '(':
        return b == ')';
    case '{':
        return b == '}';
    case '<':
        return b == '>';
    case '[':
        return b == ']';
    default:
        return false;
    }
}

bool is_closing(char c) {
    switch(c) {
    case ')':
    case '}':
    case '>':
    case ']':
        return true;
    default:
        return false;
    }
}

int corruption_value(char c) {
    switch(c) {
    case ')':
        return 3;
    case ']':
        return 57;
    case '}':
        return 1197;
    case '>':
        return 25137;
    default:
        return 0;
    }
}

stack *parse_line(char *line) {
    stack *s = init_stack();
    for (int i = 0; i < LENGTH; i++) {
        char last = pop(s);
        if (last == STACK_EMPTY_VALUE) {
            push(s, line[i]);
            continue;
        }
        if (is_pair(last, line[i])) {
            /* printf("Found a pair at %3d: %c and %c\n", i, last, line[i]); */
            /* push(s, last); */
            /* push(s, line[i]); */
        } else if(is_closing(line[i])) {
            printf("Found corruption at %3d: %c and %c\n", i, last, line[i]);
            s->corruption = corruption_value(line[i]);
            break;
        } else {
            push(s, last);
            push(s, line[i]);
        }
    }
    return s;
}

stack **parse_stacks(char **data) {
    stack **ss = malloc(sizeof(stack*) * N);
    for (int i = 0; i < N; ++i) {
        stack *s = parse_line(data[i]);
        ss[i] = s;
    }

    return ss;
}

int sum_corruption(stack **stacks) {
    int sum = 0;
    for (int i = 0; i < N; ++i) {
        sum += stacks[i]->corruption;
    }

    return sum;
}

int main(int argc, char *argv[])
{
    (void) argc;
    (void) argv;
    char** data = read_file();
    stack **stacks = parse_stacks(data);
    printf("corruption points: %d\n", sum_corruption(stacks));
    /* print_data(data); */
    return 0;
}
