#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

/* #define SAMPLE */
#ifdef SAMPLE
#define INPUT_POSITIONS 10
#define FILENAME "../sample.txt"
#else
#define INPUT_POSITIONS 1000
#define FILENAME "../input.txt"
#endif

int* get_limits(int* pos) {
    int* limits = malloc(sizeof(int) * 2);
    limits[0] = INT_MAX;
    limits[1] = INT_MIN;

    for (int i = 0; i < INPUT_POSITIONS; i++) {
        if (pos[i] > limits[1]) {
            limits[1] = pos[i];
        } else if (pos[i] < limits[0]) {
            limits[0] = pos[i];
        }
    }
    return limits;
}

int* read_file() {
    FILE *file = fopen(FILENAME, "r");

    if (file == NULL)
    {
        fprintf(stderr, "fopen error \"%s\": %s\n",
          FILENAME,
          strerror(errno));
        exit(EXIT_FAILURE);
    }

    int* pos = malloc(sizeof(int) * INPUT_POSITIONS);
    char line[4000];
    while (fgets(line, sizeof(line), file)) {

        // Remove last breakline by setting it's pointer to 0
        char *newline = strchr(line, '\n');
        if (newline) {
            *newline = 0;
        }

        char *tmp = strtok(line, ",");
        int i = 0;
        while(tmp != NULL) {
            pos[i] = atoi(tmp);
            tmp = strtok(NULL, ",");
            i++;
        }
    }

    fclose(file);
    return pos;
}

int fuel_required(int steps) {
    int fuel = 0;
    for (int i = 0; i < steps; ++i) {
        fuel += i + 1;
    }

    return fuel;
}

int get_centroid(int* pos, int* limits) {
    int min = limits[0];
    int max = limits[1];

    int centroid = min;
    int centroidSum = INT_MAX;
    for (int i = min; i <= max; ++i) {
        int sum = 0;
        for (int p = 0; p < INPUT_POSITIONS; ++p) {
            sum += fuel_required(abs(pos[p] - i));
        }

        if (sum < centroidSum) {
            centroid = i;
            centroidSum = sum;
        }
    }

    printf("Fuel Required: %d\n", centroidSum);

    return centroid;
}

void print_pos(int *pos) {
    for (int i = 0; i < INPUT_POSITIONS; ++i) {
        printf("%d ", pos[i]);
    }
    printf("\n");
}

int main(int argc, char *argv[])
{
    (void) argc;
    (void) argv;
    int* pos = read_file();
    int* limits = get_limits(pos);
    int centroid = get_centroid(pos, limits);
    printf("%d\n", centroid);
    return 0;
}
