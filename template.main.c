#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#define SAMPLE

#ifdef SAMPLE
#define FILENAME "../sample.txt"
#define N 10
#define LENGTH 10
#else
#define FILENAME "../input.txt"
#define N 100
#define LENGTH 100
#endif

char** read_file() {

    FILE *file = fopen(FILENAME, "r");

    if (file == NULL)
    {
        fprintf(stderr, "fopen error \"%s\": %s\n",
          FILENAME,
          strerror(errno));
        exit(EXIT_FAILURE);
    }

    char** lines = malloc(sizeof(char*) * N);
    char line[LENGTH * 2];
    int i = 0;

    while (fgets(line, sizeof(line), file)) {
        char *newline = strchr(line, '\n');
        if (newline) {
            *newline = 0;
        }

        lines[i] = malloc(sizeof(char) * (LENGTH + 1));
        strcpy(lines[i], line);
        i++;
    }

    return lines;
}

void print_data(char** data) {
    for (int i = 0; i < N; ++i) {
        printf("%s\n", data[i]);
    }
}

int main(int argc, char *argv[])
{
    (void) argc;
    (void) argv;
    char** data = read_file();
    print_data(data);
    return 0;
}
