#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

/* #define SAMPLE */

#ifdef SAMPLE
#define FILENAME "../sample.txt"
#define ROWS 5
#define COLUMNS 10
#else
#define FILENAME "../input.txt"
#define ROWS 100
#define COLUMNS 100
#endif

#define POINTS ROWS * COLUMNS

char** read_file() {
    FILE *file = fopen(FILENAME, "r");

    if (file == NULL)
    {
        fprintf(stderr, "fopen error \"%s\": %s\n",
          FILENAME,
          strerror(errno));
        exit(EXIT_FAILURE);
    }

    char** lines = malloc(sizeof(char*) * ROWS);
    char line[2 * COLUMNS];
    int i = 0;

    while (fgets(&line, sizeof(line), file)) {
        char *newline = strchr(line, '\n');
        if (newline) {
            *newline = 0;
        }

        lines[i] = malloc(sizeof(char) * (COLUMNS + 1));
        strcpy(lines[i], line);
        i++;
    }

    fclose(file);
    return lines;
}

int* parse_int(char** data) {
    static int hs[POINTS];
    for (int i = 0; i < ROWS; ++i) {
        for (int j = 0; j < COLUMNS; j++) {
            hs[i * COLUMNS + j] = data[i][j] - '0';
        }
    }
    return hs;
}

void print_heights(int* hs) {
    for (int i = 0; i < POINTS; ++i) {
        if (i % COLUMNS == 0)
            printf("\n");
        printf("%d ", hs[i]);
    }
    printf("\n");
}

int local_min_values(int* hs) {
    int sum = 0;
    for (int i = 0; i < POINTS; ++i) {
        int row = i / COLUMNS;
        int column = i % COLUMNS;

        // check left
        if (column != 0) {
            if (hs[i] >= hs[i - 1]) {
                continue;
            }
        }

        // check right
        if (column != (COLUMNS - 1)) {
            if (hs[i] >= hs[i + 1]) {
                continue;
            }
        }

        // check up
        if (row != 0) {
            if (hs[i] >= hs[i - COLUMNS]) {
                continue;
            }
        }

        // check down
        if (row != (ROWS - 1)) {
            if (hs[i] >= hs[i + COLUMNS]) {
                continue;
            }
        }

        sum += hs[i] + 1;
        printf("(%d, %d)\n", row, column);
    }

    return sum;
}

int main(int argc, char *argv[])
{
    char** data = read_file();
    int* hs = parse_int(data);
    /* print_heights(hs); */
    int result = local_min_values(hs);
    printf("%d\n", result);
    return 0;
}
