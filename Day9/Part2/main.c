#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

/* #define SAMPLE */

#ifdef SAMPLE
#define FILENAME "../sample.txt"
#define ROWS 5
#define COLUMNS 10
#else
#define FILENAME "../input.txt"
#define ROWS 100
#define COLUMNS 100
#endif

#define POINTS ROWS * COLUMNS
#define MAX_HEIGHT 9

char** read_file() {
    FILE *file = fopen(FILENAME, "r");

    if (file == NULL)
    {
        fprintf(stderr, "fopen error \"%s\": %s\n",
          FILENAME,
          strerror(errno));
        exit(EXIT_FAILURE);
    }

    char** lines = malloc(sizeof(char*) * ROWS);
    char line[2 * COLUMNS];
    int i = 0;

    while (fgets(line, sizeof(line), file)) {
        char *newline = strchr(line, '\n');
        if (newline) {
            *newline = 0;
        }

        lines[i] = malloc(sizeof(char) * (COLUMNS + 1));
        strcpy(lines[i], line);
        i++;
    }

    fclose(file);
    return lines;
}

int* parse_int(char** data) {
    static int hs[POINTS];
    for (int i = 0; i < ROWS; ++i) {
        for (int j = 0; j < COLUMNS; j++) {
            hs[i * COLUMNS + j] = data[i][j] - '0';
        }
    }
    return hs;
}

void print_heights(int* hs) {
    for (int i = 0; i < POINTS; ++i) {
        if (i % COLUMNS == 0)
            printf("\n");
        printf("%d ", hs[i]);
    }
    printf("\n");
}

/* Largest Possible Basin */
/*            9           */
/*           989          */
/*          98789         */
/*         9876789        */
/*        987656789       */
/*       98765456789      */
/*      9876543456789     */
/*     987654323456789    */
/*    98765432123456789   */
/*   9876543210123456789  */
/*    98765432123456789   */
/*     987654323456789    */
/*      9876543456789     */
/*       98765456789      */
/*        987656789       */
/*         9876789        */
/*          98789         */
/*           989          */
/*            9           */
// Ideia: run n times from inside out to avoid cases where it creates a spiral
int calculate_basin_size(int* hs, int min_index) {
    int* basin = malloc(sizeof(int) * POINTS);
    memset(basin, 0, sizeof(int) * POINTS);
    basin[min_index] = 1;

    /* printf("index: %d\n", min_index); */

    int n_current_members = 1;
    int n_previous_members = 0;

    while(n_current_members > n_previous_members) {
        n_previous_members = n_current_members;
        for (int i = 1; i <= MAX_HEIGHT; i++) {
            for (int rel_r = -i; rel_r <= i; rel_r++) {
                for (int rel_c = -i; rel_c <= i; rel_c++) {
                    int row = (min_index / COLUMNS) + rel_r;
                    int column = (min_index % COLUMNS) + rel_c;
                    int index = row * COLUMNS + column;

                    if (column < 0 || column >= COLUMNS) {
                        continue;
                    }

                    if (row < 0 || row >= ROWS) {
                        continue;
                    }

                    if (basin[index] == 1) {
                        continue;
                    }

                    if (hs[index] == 9) {
                        continue;
                    }

                    if (abs(rel_r) + abs(rel_c) != i)
                        continue;

                    // check left
                    if (column > 0) {
                        if (hs[index] > hs[index - 1] && basin[index - 1] == 1) {
                            basin[index] = 1;
                            n_current_members++;
                            continue;
                        }
                    }

                    // check right
                    if (column < (COLUMNS - 1)) {
                        if (hs[index] > hs[index + 1] && basin[index + 1] == 1) {
                            basin[index] = 1;
                            n_current_members++;
                            continue;
                        }
                    }

                    // check up
                    if (row > 0) {
                        if (hs[index] > hs[index - COLUMNS] && basin[index - COLUMNS] == 1) {
                            basin[index] = 1;
                            n_current_members++;
                            continue;
                        }
                    }

                    // check down
                    if (row < (ROWS - 1)) {
                        if (hs[index] > hs[index + COLUMNS] && basin[index + COLUMNS] == 1) {
                            basin[index] = 1;
                            n_current_members++;
                            continue;
                        }
                    }
                }
            }
        }
    }

    /* print_heights(basin); */
    /* printf("current basin members: %d\n", n_current_members); */
    free(basin);

    return n_current_members;
}

void sort_int(int* values) {
    int size = POINTS;
    /* printf("size %d\n", size); */
    for (int i = 0; i < size- 1; i++) {
        for (int j = i; j < size; j++) {
            if (values[j] > values[i]) {
                int tmp = values[i];
                values[i] = values[j];
                values[j] = tmp;
            }
        }
    }
}

int get_basin_product(int* hs) {
    int* basin_size_array = malloc(sizeof(int) * POINTS);
    memset(basin_size_array, 0, sizeof(int) * POINTS);

    for (int i = 0; i < POINTS; ++i) {
        int row = i / COLUMNS;
        int column = i % COLUMNS;

        // check left
        if (column != 0) {
            if (hs[i] >= hs[i - 1]) {
                continue;
            }
        }

        // check right
        if (column != (COLUMNS - 1)) {
            if (hs[i] >= hs[i + 1]) {
                continue;
            }
        }

        // check up
        if (row != 0) {
            if (hs[i] >= hs[i - COLUMNS]) {
                continue;
            }
        }

        // check down
        if (row != (ROWS - 1)) {
            if (hs[i] >= hs[i + COLUMNS]) {
                continue;
            }
        }

        basin_size_array[i] = calculate_basin_size(hs, i);
    }

    sort_int(basin_size_array);

    int product = 1;
    for (int i = 0; i < 3; ++i) {
        product *= basin_size_array[i];
    }

    return product;
}

int main(int argc, char *argv[])
{
    (void) argc;
    (void) argv;
    char** data = read_file();
    int* hs = parse_int(data);
    /* print_heights(hs); */
    int result = get_basin_product(hs);
    printf("%d\n", result);
    return 0;
}
