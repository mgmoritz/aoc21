#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

/* #define SAMPLE */

#ifdef SAMPLE
#define FILENAME "../sample.txt"
#define N1 18
#define N2 2
#define LENGTH 20
#else
#define FILENAME "../input.txt"
#define N1 983
#define N2 12
#define LENGTH 20
#endif

typedef struct point {
    int x;
    int y;
} point;

typedef struct fold {
    char axis;
    int distance;
} fold;

char** read_file() {

    FILE *file = fopen(FILENAME, "r");

    if (file == NULL)
    {
        fprintf(stderr, "fopen error \"%s\": %s\n",
          FILENAME,
          strerror(errno));
        exit(EXIT_FAILURE);
    }

    char** lines = malloc(sizeof(char*) * (N1 + N2));
    char line[LENGTH * 2];
    int i = 0;

    while (fgets(line, sizeof(line), file)) {
        char *newline = strchr(line, '\n');
        if (newline) {
            *newline = 0;
        }

        lines[i] = malloc(sizeof(char) * (LENGTH + 1));
        strcpy(lines[i], line);
        i++;
    }

    return lines;
}

void print_data(char** data) {
    for (int i = 0; i < (N1 + N2); ++i) {
        printf("%s\n", data[i]);
    }
}

point *parse_points(char** data) {
    point *pts = malloc(sizeof(point) * N1);

    for (int i = 0; i < N1; ++i) {
        pts[i].x = atoi(strtok(data[i], ","));
        pts[i].y = atoi(strtok(NULL, ","));
    }

    return pts;
}

fold *parse_folds(char** data) {
    fold *f= malloc(sizeof(fold) * N2);

    for (int i = N1 + 1; i < N1 + 1 + N2; ++i) {
        f[i - N1 - 1].axis = data[i][11];
        strtok(data[i], "=");
        f[i - N1 - 1].distance= atoi(strtok(NULL, ","));
    }

    return f;
}

void print_points(point *pts) {
    for (int i = 0; i < N1; ++i) {
        printf("%d, %d\n", pts[i].x, pts[i].y);
    }
}

void print_folds(fold *f) {
    for (int i = 0; i < N2; ++i) {
        printf("%c=%d\n", f[i].axis, f[i].distance);
    }
}

char **get_grid(point *pts, point *limits) {
    char **grid = malloc(sizeof(char*) * limits->y);
    for (int row = 0; row < limits->y; ++row) {
        grid[row] = malloc(sizeof(char) * limits->x);
        memset(grid[row], '.', sizeof(char) * limits->x);
    }

    for (int i = 0; i < N1; ++i) {
        grid[pts[i].y][pts[i].x] = '#';
    }

    return grid;
}

void print_grid(char **grid, point *limits) {
    for (int i = 0; i < limits->y; i++) {
        for (int j = 0; j < limits->x; j++) {
            printf("%c", grid[i][j]);
        }
        printf("\n");
    }
}

point *get_limits(point *pts) {
    int max_x = 0;
    int max_y = 0;
    for (int i = 0; i < N1; ++i) {
        if (pts[i].x > max_x) {
            max_x = pts[i].x;
        }

        if (pts[i].y > max_y) {
            max_y = pts[i].y;
        }
    }

    point *limits = malloc(sizeof(point));
    // zero index correction;
    limits->x = max_x + 1;
    limits->y = max_y + 1;

    return limits;
}

char **fold_x(char **grid, int distance, point *limits) {
    // get grid and limits pointer, update them to new grid and limits
    // consider fold after middle of grid
    /* printf("limits->x %d\n", limits->x); */
    int new_x = distance;
    /* printf("new_x %d\n", new_x); */
    /* printf("distance %d\n", distance); */
    char **new_grid = malloc(sizeof(char) * limits->y);

    for (int i = 0; i < limits->y; ++i) {
        new_grid[i] = malloc(sizeof(char) * new_x);
        memcpy(&new_grid[i], &grid[i], sizeof(char) * new_x);
        for (int j = 1; j < limits->x - distance; ++j) {
            /* printf("%d\n", new_x - j); */
            new_grid[i][new_x - j] = grid[i][new_x + j] == '#' || grid[i][new_x - j] == '#' ?
                '#' : '.';
        }
    }

    /* printf("%s by %d\n", "x", distance); */
    limits->x = new_x;
    /* printf("limits: %d, %d\n", limits->x, limits->y); */
    /* print_grid(new_grid, limits); */
    return new_grid;
}

char **fold_y(char **grid, int distance, point *limits) {
    int new_y = distance;
    /* printf("new_y %d\n", new_y); */
    /* printf("distance %d\n", distance); */
    char **new_grid = malloc(sizeof(char) * new_y);

    for (int i = 0; i < new_y; ++i) {
        new_grid[i] = malloc(sizeof(char) * limits->x);
        memcpy(&new_grid[i], &grid[i], sizeof(char) * limits->x);
        for (int j = 0; j < limits->x; j++) {
            if (2 * new_y - i < limits->y) {
                new_grid[i][j] = grid[i][j] == '#' || grid[2 * new_y - i][j] == '#' ?
                    '#' : '.';

            }
        }
    }

    /* printf("%s by %d\n", "y", distance); */
    limits->y = new_y;
    /* printf("limits: %d, %d\n", limits->x, limits->y); */
    /* print_grid(new_grid, limits); */
    return new_grid;
}

char **fold_grid(char **grid, fold* folds, point *limits, int max_folds) {
    // iterate over all folds and and return a char** with the new grid
    // keep the original grid unchanged
    // keep the original limits unchanged
    char **grid_after_fold = malloc(sizeof(char*) * limits->y);
    memcpy(grid_after_fold, grid, sizeof(char*) * limits->y);

    point *limits_after_fold = malloc(sizeof(point));
    memcpy(limits_after_fold, limits, sizeof(point));

    int n_folds = N2 > max_folds ? max_folds : N2;

    for (int i = 0; i < n_folds; ++i) {
        fold f = folds[i];
        /* printf("%d\n", f.distance); */
        if (f.axis == 'x') {
            fold_x(grid_after_fold, f.distance, limits_after_fold);
        } else {
            fold_y(grid_after_fold, f.distance, limits_after_fold);
        }
    }

    memcpy(limits, limits_after_fold, sizeof(point));
    free(limits_after_fold);
    return grid_after_fold;
}

int count_dots(char** grid, point *limits) {
    int result = 0;
    for (int i = 0; i < limits->y; i++) {
        for (int j = 0; j < limits->x; j++) {
            if (grid[i][j] == '#') {
                result++;
            }
        }
    }
    return result;
}

int main(int argc, char *argv[])
{
    (void) argc;
    (void) argv;
    char** data = read_file();
    /* print_data(data); */
    point *pts = parse_points(data);
    print_points(pts);
    point *limits = get_limits(pts);
    /* printf("limits %d, %d\n", limits->x, limits->y); */

    fold *folds= parse_folds(data);
    print_folds(folds);
    char** grid = get_grid(pts, limits);
    /* print_grid(grid, limits); */
    char** resulting_grid = fold_grid(grid, folds, limits, N2);
    printf("result %d\n", count_dots(resulting_grid, limits));
    print_grid(resulting_grid, limits);
    /* free(data); */
    return 0;
}
