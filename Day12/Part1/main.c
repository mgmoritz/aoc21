#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>

/* #define SAMPLE */
/* #define SMALL */
#define MAX_PATH 100000

#if defined(SAMPLE) && defined(SMALL)

#define FILENAME "../sample1.txt"
#define N 7
#define LENGTH 10

#elif defined(SAMPLE)

#define FILENAME "../sample2.txt"
#define N 18
#define LENGTH 10

#else

#define FILENAME "../input.txt"
#define N 22
#define LENGTH 10

#endif

typedef struct cave {
    char* name;
    bool seen;
    bool big;
} cave;

typedef struct cave_connection {
    char *c1;
    char *c2;
} cave_connection;

typedef struct cave_system {
    cave *caves;
    cave_connection *connections;
    int n_caves;
} cave_system;

char** read_file() {

    FILE *file = fopen(FILENAME, "r");

    if (file == NULL)
    {
        fprintf(stderr, "fopen error \"%s\": %s\n",
          FILENAME,
          strerror(errno));
        exit(EXIT_FAILURE);
    }

    char** lines = malloc(sizeof(char*) * N);
    char line[LENGTH * 2];
    int i = 0;

    while (fgets(line, sizeof(line), file)) {
        char *newline = strchr(line, '\n');
        if (newline) {
            *newline = 0;
        }

        lines[i] = malloc(sizeof(char) * (LENGTH + 1));
        strcpy(lines[i], line);
        i++;
    }

    return lines;
}

void print_data(char** data) {
    for (int i = 0; i < N; ++i) {
        printf("%s\n", data[i]);
    }
}

void print_cave(cave *c) {
    printf("Name: %s\n", c->name);
    printf("%u\n", c->seen);
    printf("%u\n", c->big);
}

void print_caves(cave_system *sys) {
    for (int i = 0; i < sys->n_caves; ++i) {
        print_cave(&sys->caves[i]);
    }
}

void print_connection(cave_connection *conn) {
    printf("%s-%s\n", conn->c1, conn->c2);
}

void print_connections(cave_connection *cc) {
    for (int i = 0; i < N; ++i) {
        print_connection(&cc[i]);
    }
}

cave_connection *parse_connections(char** data) {
    cave_connection *ccs = malloc(sizeof(cave_connection) * N);
    for (int i = 0; i < N; ++i) {
        char* n1 = strtok(data[i], "-");
        char* n2 = strtok(NULL, "-");
        ccs[i].c1 = n1;
        ccs[i].c2 = n2;
    }

    return ccs;
}

cave_system *parse_cave_system(cave_connection *ccs) {
    int n_unique_names = 0;
    char **unique_names = malloc(sizeof(char*) * N * 2);
    for (int i = 0; i < N; ++i) {
        bool unique_c1 = true;
        bool unique_c2 = true;
        for(int j = 0; j < n_unique_names; j++) {
            unique_c1 &= !(strcmp(ccs[i].c1, unique_names[j]) == 0);
            unique_c2 &= !(strcmp(ccs[i].c2, unique_names[j]) == 0);
        }
        if(unique_c1) {
            unique_names[n_unique_names] = ccs[i].c1;
            n_unique_names++;
        }
        if(unique_c2) {
            unique_names[n_unique_names] = ccs[i].c2;
            n_unique_names++;
        }
    }
    char **result = malloc(sizeof(char*) * n_unique_names);
    memcpy(result, unique_names, sizeof(char*) * n_unique_names);
    free(unique_names);

    cave_system *sys = malloc(sizeof(cave_system));
    sys->n_caves = n_unique_names;
    sys->caves = malloc(sizeof(cave) * n_unique_names);
    sys->connections = malloc(sizeof(cave_connection) * N);
    memcpy(sys->connections, ccs, sizeof(cave_connection) * N);
    for (int i = 0; i < n_unique_names; ++i) {
        sys->caves[i].name = result[i];
        sys->caves[i].seen = false;
        sys->caves[i].big = false;

        if (result[i][0] < 97)
            sys->caves[i].big = true;
    }

    return sys;
}

void reset_caves(cave_system *sys) {
    for (int i = 0; i < sys->n_caves; ++i) {
        sys->caves[i].seen = 0;
    }
}

cave *get_cave_by_name(cave *caves, char *name) {
    int i = 0;
    while(&caves[i] != NULL) {
        if (strcmp(caves[i].name, name) == 0) {
            return &caves[i];
        }
        i++;
    }

    return NULL;
}

void print_path(cave **path) {
    int i = 0;
    while(path[i] != NULL) {
        printf("%s ", path[i]->name);
        i++;
    }
    printf("\n");

}

cave_system *deep_copy_cave_system(cave_system *src) {
    cave_system *dst= malloc(sizeof(cave_system));
    dst->n_caves=src->n_caves;

    dst->caves = malloc(sizeof(cave) * src->n_caves);
    memcpy(dst->caves, src->caves, sizeof(cave) * src->n_caves);

    dst->connections = malloc(sizeof(cave_connection) * N);
    memcpy(dst->connections, src->connections, sizeof(cave_connection) * N);

    return dst;
}

bool search(cave_system *sys, char *current_name, cave **path, int step, int* result) {
    cave *current = get_cave_by_name(sys->caves, current_name);

    if (current->seen && !current->big) {
        return false;
    }

    current->seen = true;
    path[step] = current;
    step++;

    if (strcmp(current->name, "end") == 0) {
        *result += 1;
        print_path(path);
        return true;
    }

    for(int i = 0; i < N; i++) {
        if (strcmp(sys->connections[i].c1, current->name) == 0) {
            cave_system *branch = deep_copy_cave_system(sys);
            cave **path_branch = malloc(sizeof(cave) * MAX_PATH);
            memcpy(path_branch, path, sizeof(cave) * MAX_PATH);
            search(branch, sys->connections[i].c2, path_branch, step, result);
            free(branch);
            free(path_branch);
        } else if(strcmp(sys->connections[i].c2, current->name) == 0) {
            cave_system *branch = deep_copy_cave_system(sys);
            cave **path_branch = malloc(sizeof(cave) * MAX_PATH);
            memcpy(path_branch, path, sizeof(cave) * MAX_PATH);
            search(branch, sys->connections[i].c1, path_branch, step, result);
            free(branch);
            free(path_branch);
        }
    }

    return false;
}

int solve(cave_system *sys) {
    int result = 0;
    cave **path = malloc(sizeof(cave) * MAX_PATH);
    search(sys, "start", path, 0, &result);

    return result;
}

int main(int argc, char *argv[])
{
    (void) argc;
    (void) argv;
    char** data = read_file();
    cave_connection *ccs = parse_connections(data);
    cave_system *sys = parse_cave_system(ccs);
    printf("%d\n", solve(sys));
    return 0;
}
