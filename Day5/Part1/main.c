#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

/* #define SAMPLE */

#ifdef SAMPLE
#define MAX_X 10
#define MAX_Y 10
#define MAX_LINES 10
#define FILENAME "../sample.txt"
#else
#define MAX_X 1000
#define MAX_Y 1000
#define MAX_LINES 500
#define FILENAME "../input.txt"
#endif

typedef struct point {
    int x;
    int y;
} point;

typedef struct line {
    point *p1;
    point *p2;
} line;

typedef struct hvents {
    int numberOfLines;
    line* lines;
    int* overlap;
} hvents;

line* parse_line(char *inputLine) {
    char *p1String = strtok(inputLine, "->");
    char *p2String = strtok(NULL, "->");
    /* printf("%s", p1String); */
    /* printf("%s", p2String); */

    point *p1 = malloc(sizeof(point));
    p1->x = atoi(strtok(p1String, ","));
    p1->y = atoi(strtok(NULL, ","));

    /* printf("\n"); */
    /* printf("p1.x: %d\n", p1->x); */
    /* printf("p1.y: %d\n", p1->y); */

    point *p2 = malloc(sizeof(point));
    p2->x = atoi(strtok(p2String, ","));
    p2->y = atoi(strtok(NULL, ","));

    /* printf("\n"); */
    /* printf("p2.x: %d\n", p2->x); */
    /* printf("p2.y: %d\n", p2->y); */

    line *line = malloc(sizeof(line));

    line->p1 = p1;
    line->p2 = p2;

    return line;
}

hvents* init_hvents() {
    hvents *hv = malloc(sizeof(hvents));
    hv->lines = malloc(sizeof(line) * MAX_LINES);
    hv->overlap = malloc(sizeof(int) * MAX_X * MAX_Y);
    memset(hv->overlap, 0, sizeof(int) * MAX_X * MAX_Y);
    hv->numberOfLines = 0;
    return hv;
}

void print_hvents(hvents* hv) {
    printf("Lines: \n");
    for (int i = 0; i < hv->numberOfLines; ++i) {
        printf("(%d, %d) -> (%d, %d)\n",
          hv->lines[i].p1->x,
          hv->lines[i].p1->y,
          hv->lines[i].p2->x,
          hv->lines[i].p2->y);
    }

    printf("\n");
    printf("Hydrotermal Vents:\n");
    for (int i = 0; i < MAX_X; ++i) {
        for (int j = 0; j < MAX_Y; ++j) {
            printf("%d ", hv->overlap[(i * MAX_X) + j]);
        }
        printf("\n");
    }
}

void calculate_field(hvents* hv, bool hOnly) {
    for (int l = 0; l < MAX_LINES; l++) {
        int p1x = hv->lines[l].p1->x;
        int p1y = hv->lines[l].p1->y;

        int p2x = hv->lines[l].p2->x;
        int p2y = hv->lines[l].p2->y;

        if (hOnly && (p1x != p2x && p1y != p2y)) {
            continue;
        }

        int deltaX = p2x - p1x;
        int deltaY = p2y - p1y;

        int stepX = abs(deltaX) > 0
            ? deltaX / abs(deltaX)
            : 0;

        int stepY = abs(deltaY) > 0
            ? deltaY / abs(deltaY)
            : 0;

        int x = p1x;
        int y = p1y;

        hv->overlap[y * MAX_X + x] += 1;
        printf("p1: (%d, %d), p2: (%d, %d)..(%d, %d) => %d\n", p1x, p1y, p2x, p2y, x, y, y * MAX_X + x);

        while (x != p2x || y != p2y) {
            x += stepX;
            y += stepY;
            hv->overlap[y * MAX_X + x] += 1;
            printf("p1: (%d, %d), p2: (%d, %d)..(%d, %d) => %d\n", p1x, p1y, p2x, p2y, x, y, y * MAX_X + x);
        }
    }
}

int count_turbulent(hvents *hv) {
    int result = 0;
    for (int i = 0; i < MAX_X * MAX_Y; ++i) {
        if (hv->overlap[i] >= 2)
            result++;
    }

    return result;
}

hvents* read_file(char* filename) {
    FILE *file;
    file = fopen(filename, "r");

    if (file == NULL)
    {
        fprintf(stderr, "fopen error \"%s\": %s\n",
          filename,
          strerror(errno));
        exit(EXIT_FAILURE);
    }

    char inputLine[256];
    hvents *hv = init_hvents();

    while (fgets(inputLine, sizeof(inputLine), file)) {
        hv->lines[hv->numberOfLines] = *parse_line(inputLine);
        hv->numberOfLines++;
    }

    fclose(file);
    return hv;
}

int main(int argc, char *argv[])
{
    (void) argc;
    (void) argv;
    hvents *hv = read_file(FILENAME);
    calculate_field(hv, false);
    int result = count_turbulent(hv);
    print_hvents(hv);
    printf("result %d\n", result);
    return 0;
}
