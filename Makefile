.PHONY = all compile ctags run help
CFLAGS=-Wall -Wextra -std=c11 -pedantic -ggdb
DAY="1"
PART="1"
COMMAND="all"
DAY_DIR=./Day$(DAY)
DIR=./Day$(DAY)/Part$(PART)

all: new ##
	$(MAKE) -C $(DIR) $(COMMAND)

new: ## Create the file structure for a new day/part
	mkdir -p $(DIR)
	@if [ ! -f $(DIR)/main.c ]; then cp template.main.c $(DIR)/main.c; fi;
	@if [ ! -f $(DIR)/Makefile ]; then cp template.Makefile $(DIR)/Makefile; fi;
	@if [ ! -f $(DAY_DIR)/sample.txt ]; then cp template.sample.txt $(DAY_DIR)/sample.txt; fi;
	@if [ ! -f $(DAY_DIR)/input.txt ]; then cp template.input.txt $(DAY_DIR)/input.txt; fi;

help:
	@echo ""
	@echo "Usage: make all DAY=<day> PART=<part>"
	@echo ""
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/$$//' | sed -e 's/##//'
	@echo ""
